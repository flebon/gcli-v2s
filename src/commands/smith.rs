use crate::*;

use commands::identity::try_get_idty_index_by_name;
#[cfg(feature = "gdev")]
use runtime::runtime_types::gdev_runtime::opaque::SessionKeys as RuntimeSessionKeys;
use std::collections::HashMap;
use std::ops::Deref;

type SessionKeys = [u8; 128];

/// decode byte array into runtime session keys
// TODO find a way to avoid doing this manually by importing session keys trait implementation
fn session_keys_decode(session_keys: SessionKeys) -> RuntimeSessionKeys {
	RuntimeSessionKeys {
		grandpa: runtime::runtime_types::sp_consensus_grandpa::app::Public(
			runtime::runtime_types::sp_core::ed25519::Public(
				session_keys[0..32].try_into().unwrap(),
			),
		),
		babe: runtime::runtime_types::sp_consensus_babe::app::Public(
			runtime::runtime_types::sp_core::sr25519::Public(
				session_keys[32..64].try_into().unwrap(),
			),
		),
		im_online: runtime::runtime_types::pallet_im_online::sr25519::app_sr25519::Public(
			runtime::runtime_types::sp_core::sr25519::Public(
				session_keys[64..96].try_into().unwrap(),
			),
		),
		authority_discovery: runtime::runtime_types::sp_authority_discovery::app::Public(
			runtime::runtime_types::sp_core::sr25519::Public(
				session_keys[96..128].try_into().unwrap(),
			),
		),
	}
}

/// define smith subcommands
#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	/// go online
	GoOnline,
	/// go offline
	#[default]
	GoOffline,
	/// Rotate and set session keys
	UpdateKeys,
	/// Set session keys
	SetSessionKeys { session_keys: String },
	/// List upcoming expirations that require an action
	ShowExpire {
		/// Show certs that expire within less than this number of blocks
		#[clap(short, long, default_value_t = 100800)]
		blocks: u32,
		/// Show authorities that should rotate keys within less than this number of sessions
		#[clap(short, long, default_value_t = 100)]
		sessions: u32,
	},
	/// List online authorities
	ShowOnline,
	/// Invite identity to become smith
	Invite { target: String },
	/// Accept invitation
	Accept,
	/// Certify smith
	#[clap(alias = "cert")]
	Certify { target: String },
}

/// handle smith commands
pub async fn handle_command(data: Data, command: Subcommand) -> Result<(), GcliError> {
	let mut data = data.build_client().await?.build_indexer().await?;
	match command {
		Subcommand::GoOnline => {
			go_online(&data).await?;
		}
		Subcommand::GoOffline => {
			go_offline(&data).await?;
		}
		Subcommand::UpdateKeys => {
			update_session_keys(&data).await?;
		}
		Subcommand::SetSessionKeys { session_keys } => {
			let session_keys = session_keys_decode(
				hex::decode(session_keys)
					.expect("wrong hexadecimal")
					.try_into()
					.expect("wrong format"),
			); // decode session keys from hex string
			set_session_keys(&data, session_keys).await?;
		}
		Subcommand::ShowExpire { blocks, sessions } => {
			data = data.build_indexer().await?;
			commands::expire::monitor_expirations(&data, blocks, sessions).await?
		}
		Subcommand::ShowOnline => online(&data).await?,
		Subcommand::Invite { target } => {
			let target = try_get_idty_index_by_name(&data, &target).await?;
			invite_smith(&data, target).await?
		}
		Subcommand::Accept => accept_invitation(&data).await?,
		Subcommand::Certify { target } => {
			let target = try_get_idty_index_by_name(&data, &target).await?;
			certify_smith(&data, target).await?
		}
	};

	Ok(())
}

/// rotate session keys
/// (needs to be connected to unsafe RPC)
pub async fn rotate_keys(data: &Data) -> Result<SessionKeys, anyhow::Error> {
	data.legacy_rpc_methods()
		.await
		.author_rotate_keys()
		.await
		.map_err(|e| {
			anyhow!(
				"Please make sure you are connected to your validator node with the unsafe RPC \
				 API enabled {e}"
			)
		})?
		.deref()
		.try_into()
		.map_err(|e| anyhow!("Session keys have wrong length: {:?}", e))
}

/// set session keys
pub async fn set_session_keys(
	data: &Data,
	session_keys: RuntimeSessionKeys,
) -> Result<TxProgress, subxt::Error> {
	submit_call::<Payload<runtime::authority_members::calls::types::SetSessionKeys>>(
		data,
		&runtime::tx()
			.authority_members()
			.set_session_keys(session_keys),
	)
	.await
}

// use runtime::runtime_types::sp_consensus_grandpa::app::Public

/// update session keys
pub async fn update_session_keys(data: &Data) -> Result<(), GcliError> {
	let session_keys = rotate_keys(data).await?;
	// manual session key conversion
	let session_keys = session_keys_decode(session_keys);
	let progress = set_session_keys(data, session_keys).await?;

	if data.args.no_wait {
		return Ok(());
	}
	let _ = track_progress(progress).await?; // TODO
	Ok(())
}

/// submit go_online
pub async fn go_online(data: &Data) -> Result<(), GcliError> {
	if data
		.client()
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().session().next_keys(data.address()))
		.await?
		.is_none()
	{
		return Err(GcliError::Logic(
			"This account has not set session keys!".to_string(),
		));
	}

	submit_call_and_look_event::<
		runtime::authority_members::events::MemberGoOnline,
		Payload<runtime::authority_members::calls::types::GoOnline>,
	>(data, &runtime::tx().authority_members().go_online())
	.await
	.map_err(|e| e.into())
}

/// submit go_offline
pub async fn go_offline(data: &Data) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::authority_members::events::MemberGoOffline,
		Payload<runtime::authority_members::calls::types::GoOffline>,
	>(data, &runtime::tx().authority_members().go_offline())
	.await
}

/// get online authorities
pub async fn online(data: &Data) -> Result<(), anyhow::Error> {
	let client = data.client();

	let online_authorities = client
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().authority_members().online_authorities())
		.await?
		.unwrap_or_default();
	let incoming_authorities = client
		.storage()
		.at_latest()
		.await?
		.fetch(
			&runtime::storage()
				.authority_members()
				.incoming_authorities(),
		)
		.await?
		.unwrap_or_default();
	let outgoing_authorities = client
		.storage()
		.at_latest()
		.await?
		.fetch(
			&runtime::storage()
				.authority_members()
				.outgoing_authorities(),
		)
		.await?
		.unwrap_or_default();

	if let Some(indexer) = &data.indexer {
		let mut names = HashMap::<IdtyId, String>::new();
		indexer
			.names_by_indexes(&online_authorities)
			.await
			.into_iter()
			.for_each(|e| {
				names.insert(e.0, e.1);
			});
		println!("Online:");
		println!(
			"{}",
			online_authorities
				.iter()
				.map(|i| &names
					.get(i)
					.expect("panic! found authorities whith no name")[..])
				.collect::<Vec<&str>>()
				.join(", ")
		);

		println!("Incoming:");
		println!(
			"{}",
			incoming_authorities
				.iter()
				.map(|i| &names
					.get(i)
					.expect("panic! found authorities whith no name")[..])
				.collect::<Vec<&str>>()
				.join(", ")
		);

		println!("Outgoing:");
		println!(
			"{}",
			outgoing_authorities
				.iter()
				.map(|i| &names
					.get(i)
					.expect("panic! found authorities whith no name")[..])
				.collect::<Vec<&str>>()
				.join(", ")
		);
	} else {
		println!("Online:");
		println!("{online_authorities:?}");

		println!("Incoming:");
		println!("{incoming_authorities:?}");

		println!("Outgoing:");
		println!("{outgoing_authorities:?}");
	}

	Ok(())
}

/// invite identity to join smith
pub async fn invite_smith(data: &Data, target: IdtyId) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::smith_members::events::InvitationSent,
		Payload<runtime::smith_members::calls::types::InviteSmith>,
	>(data, &runtime::tx().smith_members().invite_smith(target))
	.await
}

/// accept invitation
pub async fn accept_invitation(data: &Data) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::smith_members::events::InvitationAccepted,
		Payload<runtime::smith_members::calls::types::AcceptInvitation>,
	>(data, &runtime::tx().smith_members().accept_invitation())
	.await
}

/// invite identity to join smith
pub async fn certify_smith(data: &Data, target: IdtyId) -> Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::smith_members::events::SmithCertAdded,
		Payload<runtime::smith_members::calls::types::CertifySmith>,
	>(data, &runtime::tx().smith_members().certify_smith(target))
	.await
}
