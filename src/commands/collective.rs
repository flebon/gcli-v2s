use crate::*;

/// define technical committee subcommands
#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	#[default]
	/// List members of the technical committee
	Members,
	/// Propose a hex encoded call
	Propose { hex: String },
	/// List proposals to the technical committee
	Proposals,
	/// Vote a proposal to the technical committee
	Vote {
		/// Proposal hash
		hash: Hash,
		/// Proposal index
		index: u32,
		/// Vote (0=against, 1=for)
		vote: u8,
	},
}

/// handle technical committee commands
pub async fn handle_command(data: Data, command: Subcommand) -> anyhow::Result<(), GcliError> {
	let data = data.build_client().await?.build_indexer().await?;
	match command {
		Subcommand::Members => technical_committee_members(&data).await?,
		Subcommand::Propose { hex } => technical_committee_propose(&data, &hex).await?,
		Subcommand::Proposals => technical_committee_proposals(data.client()).await?,
		Subcommand::Vote { hash, index, vote } => {
			let vote = match vote {
				0 => false,
				1 => true,
				_ => panic!("Vote must be written 0 if you disagree, or 1 if you agree."),
			};
			technical_committee_vote(
				&data, hash, //Hash::from_str(&hash).expect("Invalid hash formatting"),
				index, vote,
			)
			.await?;
		}
	};

	Ok(())
}

/// list technical committee members
pub async fn technical_committee_members(data: &Data) -> Result<(), anyhow::Error> {
	let client = data.client();
	let indexer = &data.indexer;

	for account_id in client
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().technical_committee().members())
		.await?
		.unwrap_or_default()
	{
		println!(
			"{}",
			if let Some(indexer) = indexer {
				// indexer is set, we can get the name
				let name = indexer.username_by_pubkey(&account_id.to_string()).await;
				if name.is_some() {
					name
				} else {
					indexer
						.wasname_by_pubkey(&account_id.to_string())
						.await
						.map(|name| format!("{name}\t(old account)"))
				}
			} else {
				// indexer is not set, we can get the idty index by accountid
				client
					.storage()
					.at_latest()
					.await?
					.fetch(&runtime::storage().identity().identity_index_of(&account_id))
					.await?
					.map(|identity_id| format!("{identity_id}"))
			}
			// no idty found, display account_id
			.unwrap_or_else(|| account_id.to_string(),)
		);
	}

	Ok(())
}

/// list technical committee proposals
// TODO:
// * better formatting (format pubkeys to SS58 and add usernames)
// * display proposals indices
pub async fn technical_committee_proposals(client: &Client) -> anyhow::Result<()> {
	let parent_hash = client
		.storage()
		.at_latest()
		.await?
		.fetch(&runtime::storage().system().parent_hash())
		.await?
		.unwrap();

	let mut proposals_iter = client
		.storage()
		.at(parent_hash)
		.iter(runtime::storage().technical_committee().proposal_of_iter())
		.await?;
	while let Some(Ok((proposal_hash, proposal))) = proposals_iter.next().await {
		println!("{}", hex::encode(&proposal_hash[32..64]));
		println!("{proposal:#?}");
		println!();
	}

	Ok(())
}

/// submit vote to technical committee
pub async fn technical_committee_vote(
	data: &Data,
	proposal_hash: Hash,
	proposal_index: u32,
	vote: bool,
) -> anyhow::Result<(), subxt::Error> {
	submit_call_and_look_event::<
		runtime::technical_committee::events::Voted,
		Payload<runtime::technical_committee::calls::types::Vote>,
	>(
		data,
		&runtime::tx()
			.technical_committee()
			.vote(proposal_hash, proposal_index, vote),
	)
	.await
}

/// propose call given as hexadecimal
/// can be generated with `subxt explore` for example
pub async fn technical_committee_propose(
	data: &Data,
	proposal: &str,
) -> anyhow::Result<(), subxt::Error> {
	let raw_call = hex::decode(proposal).expect("invalid hex");
	let call = codec::decode_from_bytes(raw_call.into()).expect("invalid call");
	let payload = runtime::tx().technical_committee().propose(5, call, 100);

	submit_call_and_look_event::<
		runtime::technical_committee::events::Proposed,
		Payload<runtime::technical_committee::calls::types::Propose>,
	>(data, &payload)
	.await
}
