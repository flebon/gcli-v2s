use crate::*;
use bs58;

/// define cesium subcommands
#[derive(Clone, Default, Debug, clap::Parser)]
pub enum Subcommand {
	// Nothing
	#[default]
	#[clap(hide = true)]
	Nothing,
	/// Analyse a base58 pubkey and gives it in all its form
	Pubkey { pubkey: String },
	/// Prompt for cesium input
	Prompt,
}

/// handle blockchain commands
pub async fn handle_command(_data: Data, command: Subcommand) -> Result<(), GcliError> {
	match command {
		Subcommand::Nothing => {}
		Subcommand::Pubkey { pubkey } => {
			let raw_pubkey = bs58::decode(pubkey).into_vec().unwrap();
			let raw_pubkey: [u8; 32] = if raw_pubkey.len() > 32 {
				return Err(GcliError::Input("invalid pubkey size".to_string()));
			} else {
				[vec![0; 32 - raw_pubkey.len()], raw_pubkey]
					.concat()
					.try_into()
					.unwrap()
			};
			println!("Pubkey (hex): 0x{}", hex::encode(raw_pubkey));
			let address: AccountId = sp_core::ed25519::Public(raw_pubkey).into();
			println!("Address (SS58): {}", address);
		}
		Subcommand::Prompt => {
			let keypair = prompt_secret_cesium();
			println!("Pubkey: {}", bs58::encode(keypair.pkey).into_string());
			let address: AccountId = keypair.pkey.into();
			println!("Address: {}", address);
		}
	}
	Ok(())
}

pub struct CesiumSigner<T: subxt::Config> {
	account_id: T::AccountId,
	keypair: nacl::sign::Keypair,
}
impl<T> CesiumSigner<T>
where
	T: subxt::Config,
	T::AccountId: From<[u8; 32]>,
{
	pub fn new(keypair: nacl::sign::Keypair) -> Self {
		Self {
			account_id: T::AccountId::from(keypair.pkey),
			keypair,
		}
	}
}
impl<T> subxt::tx::Signer<T> for CesiumSigner<T>
where
	T: subxt::Config,
	T::Address: From<T::AccountId>,
	T::Signature: From<sp_core::ed25519::Signature>,
{
	fn account_id(&self) -> T::AccountId {
		self.account_id.clone()
	}

	fn address(&self) -> T::Address {
		self.account_id.clone().into()
	}

	fn sign(&self, payload: &[u8]) -> T::Signature {
		sp_core::ed25519::Signature(
			nacl::sign::signature(payload, &self.keypair.skey)
				.unwrap()
				.try_into()
				.expect("could not read signature"),
		)
		.into()
	}
}
