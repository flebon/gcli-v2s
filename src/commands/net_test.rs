use crate::*;

use sp_core::DeriveJunction;
use subxt::ext::sp_runtime::MultiAddress;

pub async fn repart(data: &Data, target: u32, actual_repart: Option<u32>) -> anyhow::Result<()> {
	let KeyPair::Sr25519(keypair) = data.keypair() else {
		panic!("Cesium keys not implemented there")
	};
	let mut pairs = Vec::new();
	for i in actual_repart.unwrap_or_default()..target {
		let pair_i = keypair
			.derive(std::iter::once(DeriveJunction::hard::<u32>(i)), None)
			.map_err(|_| anyhow!("Fail to derive //{}", i))?
			.0;
		pairs.push((i, pair_i));
	}

	for (i, pair_i) in &pairs {
		/*let _ = api
			.tx()
			.balances()
			.transfer_allow_death(MultiAddress::Id(pair_i.public().into()), 501)?
			.sign_and_submit_then_watch(&signer, DefaultExtrinsicParamsBuilder::new())
			.await?
			.wait_for_in_block()
			.await?;
		signer.increment_nonce();*/

		if let Some(pair_i_account) = data
			.client()
			.storage()
			.at_latest()
			.await?
			.fetch(&runtime::storage().system().account(&pair_i.public().into()))
			.await?
		{
			log::info!("account //{} balance: {}", i, pair_i_account.data.free);
		}
	}

	Ok(())
}

pub async fn spam_roll(data: &Data, actual_repart: usize) -> anyhow::Result<()> {
	let KeyPair::Sr25519(keypair) = data.keypair() else {
		panic!("Cesium keys not implemented there")
	};
	let client = data.client();
	let mut nonce = 0;
	let mut pairs =
		Vec::<(PairSigner<Runtime, sr25519::Pair>, AccountId)>::with_capacity(actual_repart);
	for i in 0..actual_repart {
		let pair_i = keypair
			.derive(std::iter::once(DeriveJunction::hard::<u32>(i as u32)), None)
			.map_err(|_| anyhow!("Fail to derive //{}", i))?
			.0;
		let account_id_i = pair_i.public().into();
		pairs.push((PairSigner::new(pair_i), account_id_i));
	}

	loop {
		let mut watchers = Vec::with_capacity(actual_repart);
		for i in 0..(actual_repart - 1) {
			let dest: AccountId = pairs[i + 1].1.clone();
			let watcher = client
				.tx()
				.create_signed_with_nonce(
					&runtime::tx()
						.balances()
						.transfer_allow_death(MultiAddress::Id(dest).into(), 1),
					&pairs[i].0,
					nonce,
					DefaultExtrinsicParamsBuilder::new().build(),
				)?
				.submit_and_watch()
				.await?;
			nonce += 1;
			log::info!("send 1 cent from //{} to //{}", i, i + 1);
			watchers.push(watcher);
		}
		let dest: AccountId = pairs[0].1.clone();
		let watcher = client
			.tx()
			.sign_and_submit_then_watch(
				&runtime::tx()
					.balances()
					.transfer_allow_death(MultiAddress::Id(dest).into(), 1),
				&pairs[actual_repart - 1].0,
				DefaultExtrinsicParamsBuilder::new().build(),
			)
			.await?;
		nonce += 1;
		log::info!("send 1 cent from //{} to //0", actual_repart - 1);
		watchers.push(watcher);

		// Wait all transactions
		// FIXME fix after subxt update
		// for watcher in watchers {
		// 	watcher.wait_for_in_block().await?;
		// }
	}
}
